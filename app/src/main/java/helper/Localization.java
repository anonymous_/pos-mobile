package helper;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ASUS on 02/11/2017.
 */

public class Localization {

    public Date getTime() {        
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public String stringDate(Date date) {
        DateFormat dateFormat = new DateFormat();
        return dateFormat.format("yyyy-MM-dd", date).toString();
    }

    public String stringDateTime(Date date) {
        DateFormat dateFormat = new DateFormat();
        return dateFormat.format("yyyy-MM-dd HH:mm:ss", date).toString();
    }

    public String stringDate(String date) {
        String[] parse = date.split("-");
        if (parse.length == 3) {
            return parse[2] + "-" + parse[1] + "-" + parse[0];
        } else {
            return "";
        }
    }

    public String humanDate(String str) {
        String[] parse = str.split("-");
        return parse[2]+" "+month(parse[1])+" "+parse[0];
    }

    public String humanDateTime(String str) {
        String[] parse = str.split(" ");
        String[] dateParse = parse[0].split("-");
        return dateParse[2]+" "+month(dateParse[1])+" "+dateParse[0]+" "+parse[1];
    }

    public String month (String m) {
        Map<String, String> months = new HashMap<String, String>();
        months.put("01", "Januari");
        months.put("02", "Febuari");
        months.put("03", "Maret");
        months.put("04", "April");
        months.put("05", "Mei");
        months.put("06", "Juni");
        months.put("07", "Juli");
        months.put("08", "Agustus");
        months.put("09", "September");
        months.put("10", "Oktober");
        months.put("11", "November");
        months.put("12", "Desember");
        return months.get(m);
    }
}
