package fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapter.ListProduk;
import adapter.ProdukAdapter;
import helper.Alert;
import config.Utils;
import project.pos.MainLoginActivity;
import project.pos.MainReturActivity;
import project.pos.R;

public class HomeFragment extends BaseFragment implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    View view;
    LinearLayout layoutBottomSheet;
    BottomSheetBehavior behavior;
    BottomNavigationView navigationView;
    Boolean menu = false;

    private RecyclerView recyclerView;
    private ProdukAdapter produkAdapter;
    private List<ListProduk> listProduks;

    private TextView tvTb, tvTtl, tvPjk, tvSubTtl;
    private EditText etDisc;
    EditText et_bayar, et_kembali;
    DecimalFormat dec = new DecimalFormat("#");
    private boolean isPersen = false;
    BroadcastReceiver broadcastReceiver;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initView();
        setListener();
        return view;
    }

    private void initView(){
        navigationView      = view.findViewById(R.id.nav_bottom);
        layoutBottomSheet   = view.findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(layoutBottomSheet);

        tvTb    = view.findViewById(R.id.tv_tb);
        tvTtl   = view.findViewById(R.id.tv_ttl);
        tvPjk   = view.findViewById(R.id.tv_pjk);
        tvSubTtl= view.findViewById(R.id.tv_subTtl);
        etDisc  = view.findViewById(R.id.et_disc);

        recyclerView = view.findViewById(R.id.recycleProduk);
        recyclerView.setHasFixedSize(true);
        listProduks = new ArrayList<>();
    }

    private void setListener(){
        navigationView.setOnNavigationItemSelectedListener(this);

//        tvUser.setText(" Nama \t\t\t : "+loginPrefs.getString("fullname")
//                +" \n Email \t\t\t : "+loginPrefs.getString("email")
//                +" \n Role Id \t\t : "+loginPrefs.getString("role_id")
//                +" \n Outlet Id \t : "+loginPrefs.getString("outlet_id"));

        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                if(newState == BottomSheetBehavior.STATE_EXPANDED){
                    if (!menu) {
                        menu = true;
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.frameSheet, new HomeAllFragment(),
                                        Utils.Home_Content_Fragment).commit();
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        etDisc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                float disc;
                float total = Integer.parseInt(tvTtl.getText().toString());
                if (s.toString().isEmpty())
                    disc = total - 0;
                else{
                    if (isPersen){
                        if (Float.parseFloat(s.toString()) > 100){
                            new Alert().Error(getContext(), view, "Diskon melebihi 100%");
                            etDisc.setText("100");
                            disc = 100/100*total;
                        }else if(Float.parseFloat(s.toString()) < 0){
                            new Alert().Error(getContext(), view, "Diskon kurang dari 0%");
                            etDisc.setText("");
                            disc = total - 0;
                        }else{
                            disc = Float.parseFloat(s.toString())/100*total;
                        }
                        disc = total - disc;
                    }else{
                        if (Float.parseFloat(s.toString()) > total) {
                            new Alert().Error(getContext(), view, "Diskon melebihi total harga pemebelian");
                            etDisc.setText(String.valueOf(dec.format(total)));
                            disc = total - total;
                        }else if(Float.parseFloat(s.toString()) < 0){
                            new Alert().Error(getContext(), view, "Diskon kurang dari 0");
                            disc = total - 0;
                        }else{
                            disc = total - Float.parseFloat(s.toString());
                        }
                    }
                }
                tvSubTtl.setText(String.valueOf(dec.format(disc)));
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        view.findViewById(R.id.tvPersen).setOnClickListener(this);
        view.findViewById(R.id.btnBatal).setOnClickListener(this);
        view.findViewById(R.id.btnBayar).setOnClickListener(this);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case "addProduk":
                        addProduk(intent.getStringExtra("id")
                                , intent.getStringExtra("produk")
                                , intent.getStringExtra("code")
                                , intent.getStringExtra("harga"), "1");
                        break;
                    case "plusProduk":
                        plusminProduk(intent.getStringExtra("id"), true);
                        break;
                    case "minProduk":
                        plusminProduk(intent.getStringExtra("id"), false);
                        break;
                    case "removeProduk":
                        removeProduk(intent.getStringExtra("id"));
                        break;
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter("addProduk");
        intentFilter.addAction("plusProduk");
        intentFilter.addAction("minProduk");
        intentFilter.addAction("removeProduk");
        getContext().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvPersen:
                float disc;
                float total = Integer.parseInt(tvTtl.getText().toString());
                if (!isPersen) {
                    view.findViewById(R.id.tvPer).setVisibility(View.VISIBLE);
                    isPersen = true;
                    if (etDisc.getText().toString().isEmpty())
                        disc = total - 0;
                    else {
                        if (Float.parseFloat(etDisc.getText().toString()) > 100) {
                            etDisc.setText("100");
                            disc = 100 / 100 * total;
                        } else if (Float.parseFloat(etDisc.getText().toString()) < 0) {
                            etDisc.setText("");
                            disc = total - 0;
                        } else {
                            disc = (Float.parseFloat(etDisc.getText().toString()) / 100)* total;
                        }
                        disc = total - disc;
                    }
                    tvSubTtl.setText(String.valueOf(dec.format(disc)));
                }else{
                    view.findViewById(R.id.tvPer).setVisibility(View.GONE);
                    isPersen = false;
                    if (etDisc.getText().toString().isEmpty())
                        disc = total - 0;
                    else {
                        if (Float.parseFloat(etDisc.getText().toString()) > total) {
                            new Alert().Error(getContext(), view, "Diskon melebihi total harga pemebelian");
                            etDisc.setText(String.valueOf(dec.format(total)));
                            disc = total - total;
                        }else if(Float.parseFloat(etDisc.getText().toString()) < 0){
                            new Alert().Error(getContext(), view, "Diskon kurang dari 0");
                            disc = total - 0;
                        }else{
                            disc = total - Float.parseFloat(etDisc.getText().toString());
                        }
                    }
                    tvSubTtl.setText(String.valueOf(dec.format(disc)));
                }
                break;
            case R.id.btnBatal:
                listProduks.clear();
                showProduk();
                break;
            case R.id.btnBayar:
                if (listProduks.size() > 0)
                    modalBayar();
                else
                    new Alert().Error(getContext(), v, "Tidak ada pembelian yang dapat di proses");
                break;
            case R.id.btnPebayaran:
                Float bayar = (et_bayar.getText().toString().isEmpty())?0:Float.parseFloat(et_bayar.getText().toString());
                if (bayar > 0 && Float.parseFloat(et_kembali.getText().toString()) <= bayar)
                    bayarProduk();
                else
                    new Alert().Error(getContext(), v, "Pembayaran kurang dari total yang harus di bayar");
                break;
            case R.id.close:
                if (dialog.isShowing())
                    dialog.dismiss();
                break;
            case R.id.btnMenuRetur:
                hideDialog();
                startActivity(new Intent(getContext(), MainReturActivity.class));
                break;
            case R.id.btnMenuLogout:
                loginPrefs.destroyPreferences();
                startActivity(new Intent(getContext(), MainLoginActivity.class).putExtra("logout", true));
                ((Activity)getActivity()).finishAffinity();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment;
        Bundle arg = new Bundle();
        switch (item.getItemId()){
            case R.id.item1:
                menu = true;
                item.setChecked(true);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, new HomeAllFragment(),
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item2:
                menu = true;
                item.setChecked(true);
                fragment = new HomeAllFragment();
                arg.putString("category", "10");
                fragment.setArguments(arg);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, fragment,
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item3:
                menu = true;
                item.setChecked(true);
                fragment = new HomeAllFragment();
                arg.putString("category", "11");
                fragment.setArguments(arg);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, fragment,
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item4:
                menu = true;
                item.setChecked(true);
                fragment = new HomeAllFragment();
                arg.putString("category", "12");
                fragment.setArguments(arg);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameSheet, fragment,
                                Utils.Home_Content_Fragment).commit();
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.item5:
                if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                else{
                    modalMenu();
                }
                break;
        }
        return false;
    }

    private void showProduk(){
        produkAdapter = new ProdukAdapter(getContext(), listProduks);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(produkAdapter);

        int S_tb = 0, S_ttl = 0;
        for (int i = 0; i < listProduks.size(); i++){
            S_tb  += Integer.parseInt(listProduks.get(i).getQty());
            S_ttl += Float.parseFloat(listProduks.get(i).getQty())*Float.parseFloat(listProduks.get(i).getHarga());
        }

        tvTb.setText(String.valueOf(S_tb));
        tvTtl.setText(String.valueOf(dec.format(S_ttl)));
        tvSubTtl.setText(String.valueOf(dec.format(S_ttl)));
    }

    private void addProduk(String id, String nama, String code, String harga, String qty){
        harga = String.valueOf(dec.format(Float.parseFloat(harga)));
        boolean isProduk = false;
        for (int i = 0; i < listProduks.size(); i++){
            if (!isProduk && listProduks.get(i).getId_produk().equals(id)){
                int qtyPlus = Integer.parseInt(listProduks.get(i).getQty())+1;
                checkUpdateQty(i, id, listProduks.get(i).getNama()
                        , listProduks.get(i).getCode(), listProduks.get(i).getHarga(), qtyPlus);
                isProduk = true;
            }
        }
        if(!isProduk){
            checkAddQty(id, nama, code, harga, Integer.parseInt(qty));
        }
    }

    private void plusminProduk(String id, boolean plus){
        for (int i = 0; i < listProduks.size(); i++){
            if (listProduks.get(i).getId_produk().equals(id)){
                int qty;
                if (plus)
                    qty = Integer.parseInt(listProduks.get(i).getQty())+1;
                else {
                    qty = Integer.parseInt(listProduks.get(i).getQty()) - 1;
                }
                if (qty >= 1)
                    checkUpdateQty(i, id, listProduks.get(i).getNama()
                            , listProduks.get(i).getCode(), listProduks.get(i).getHarga(), qty);
                else
                    new Alert().Error(getContext(), view, "Quantity harus lebih dari 0");
            }
        }
    }

    private void removeProduk(String id){
        for (int i = 0; i < listProduks.size(); i++){
            if (listProduks.get(i).getId_produk().equals(id)){
                listProduks.remove(i);
            }
        }
        showProduk();
    }

    private void checkAddQty(final String id, final String nama, final String code, final String harga, final int qty){
        request = new StringRequest(Request.Method.POST, routing.getQty, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Qty", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        JSONObject data = jsonResponse.getJSONObject("data");
                        if (qty <= data.getInt("qty")) {
                            listProduks.add(new ListProduk(id, nama, code, harga, String.valueOf(qty)));
                            showProduk();
                        }else
                            new Alert().Error(getContext(), view, "Stok Habis !");
                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("produk_id", id);
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                return params;
            }
        };
        httpRequest.addQueue(request);
    }

    private void checkUpdateQty(final int set, final String id, final String nama, final String code, final String harga, final int qty){
        request = new StringRequest(Request.Method.POST, routing.getQty, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Qty", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        JSONObject data = jsonResponse.getJSONObject("data");
                        if (qty <= data.getInt("qty")) {
                            listProduks.set(set, new ListProduk(id, nama, code, harga, String.valueOf(qty)));
                            showProduk();
                        }else
                            new Alert().Error(getContext(), view, "Stok Habis !");
                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("produk_id", id);
                params.put("outlet_id", loginPrefs.getString("outlet_id"));
                return params;
            }
        };

        httpRequest.addQueue(request);
    }

    private void bayarProduk(){
        request = new StringRequest(Request.Method.POST, routing.bayar, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Bayar", response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success){
                        JSONObject data = jsonResponse.getJSONObject("data");
                        new Alert().Success(getContext(), view, jsonResponse.getString("message"));
                        if (dialog.isShowing())
                            dialog.dismiss();
                        listProduks.clear();
                        showProduk();
                        Bundle args = new Bundle();
                        args.putString("url", data.getString("url"));
                        args.putString("urlpdf", data.getString("urlPdf"));
                        Fragment fragment = new InvoiceFragment();
                        fragment.setArguments(args);
                        fragmentManager
                                .beginTransaction().setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                .replace(R.id.frameContainer, fragment,
                                        Utils.Invoice_Fragment).commit();
                    }else{
                        new Alert().Error(getContext(), view, jsonResponse.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                for (int i = 0; i < listProduks.size(); i++) {
                    params.put("dataProduk["+i+"][id]", listProduks.get(i).getId_produk());
                    params.put("dataProduk["+i+"][qty]", listProduks.get(i).getQty());
                }
                params.put("isProduk", (listProduks.size()>0)?"true":"false");
                if (listProduks.size() > 0) {
                    params.put("paid_amt", et_bayar.getText().toString());
                    params.put("return_change", et_kembali.getText().toString());
                    params.put("payment_method", "1");
                    params.put("total_items", tvTb.getText().toString());
                    params.put("grandtotal", tvSubTtl.getText().toString());
                    params.put("tax", "0");
                    params.put("discount_percentage", (isPersen) ? etDisc.getText().toString() : "");
                    params.put("discount_total", (!isPersen) ? etDisc.getText().toString() : "0");
                    params.put("subtotal", tvTtl.getText().toString());
                    params.put("outlet_id", loginPrefs.getString("outlet_id"));
                    params.put("ordered_datetime", localization.stringDateTime(localization.getTime()));
                    params.put("customer_id", "1");
                    params.put("created_user_id", loginPrefs.getString("id"));
                }
                return params;
            }
        };

        httpRequest.addQueue(request);
    }

    private void modalMenu() {
        if (dialog.isShowing())
            dialog.dismiss();

        dialog.setContentView(R.layout.modal_menu);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.findViewById(R.id.close).setOnClickListener(this);
        dialog.findViewById(R.id.btnMenuRetur).setOnClickListener(this);
        dialog.findViewById(R.id.btnMenuLogout).setOnClickListener(this);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void modalBayar(){
        if (dialog.isShowing())
            dialog.dismiss();

        dialog.setContentView(R.layout.modal_pebayaran);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.findViewById(R.id.close).setOnClickListener(this);
        dialog.findViewById(R.id.btnPebayaran).setOnClickListener(this);
        EditText et_ttlBayar    = dialog.findViewById(R.id.total_bayar);
        EditText et_ttlBarang   = dialog.findViewById(R.id.total_barang);
        et_bayar       = dialog.findViewById(R.id.jumlah_bayar);
        et_kembali     = dialog.findViewById(R.id.kembalian);
        et_ttlBayar.setText(tvTtl.getText());
        et_ttlBarang.setText(tvTb.getText());

        et_bayar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int total = Integer.parseInt(tvTtl.getText().toString());
                if (s.toString().isEmpty())
                    et_kembali.setText("0");
                else{
                    int kembalian = Integer.parseInt(s.toString())- total;
                    et_kembali.setText(String.valueOf(kembalian));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

}
