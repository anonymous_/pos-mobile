package fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import config.Utils;
import project.pos.R;

public class InvoiceFragment extends BaseFragment implements View.OnClickListener {
    TextView tvTitle;
    private WebView web;
    public InvoiceFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_invoice, container, false);
        initView(view);
        return view;
    }

    public void initView(View v){
        v.findViewById(R.id.ivBack).setOnClickListener(this);
        v.findViewById(R.id.ivPdf).setOnClickListener(this);
        web = v.findViewById(R.id.web_view);
        web.loadUrl(getArguments().getString("url"));
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        web.getSettings().setEnableSmoothTransition(true);
        web.setWebChromeClient(new WebChromeClient());
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
//                    new CustomToast().Show_Toast_Success(getActivity(), v, "Back");
                    web.goBack();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBack:
                fragmentManager
                        .beginTransaction().setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                        .replace(R.id.frameContainer, new HomeFragment(),
                                Utils.Home_Fragment).commit();
                break;
            case R.id.ivPdf:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getArguments().getString("urlpdf"))));
                break;
        }
    }
}
