package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import project.pos.R;

public class ProdukAdapter extends RecyclerView.Adapter<ProdukAdapter.ViewHolder> {

    private Context context;
    private List<ListProduk> adapterList;
    public ProdukAdapter(Context context, List<ListProduk> adapterList){
        this.context = context; this.adapterList = adapterList;
    }

    @NonNull
    @Override
    public ProdukAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.single_penjualan, null);
        return new ProdukAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProdukAdapter.ViewHolder holder, int position) {
        ListProduk data = adapterList.get(position);
        holder.id = data.getId_produk();
        holder.nama.setText(data.getNama()+" ["+data.getCode()+"]");
        int harga = Integer.parseInt(data.getQty())*Integer.parseInt(data.getHarga());
        holder.harga.setText(String.valueOf(harga));
        holder.qty.setText(data.getQty());

    }

    @Override
    public int getItemCount() {
        return adapterList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nama, harga, qty;
        String id;
        ImageView delete, min, plus;

        public ViewHolder(final View itemView){
            super(itemView);
            nama    = itemView.findViewById(R.id.tvProduk);
            harga   = itemView.findViewById(R.id.tvHrg);
            qty     = itemView.findViewById(R.id.tvQty);
            delete  = itemView.findViewById(R.id.ivDel);
            min     = itemView.findViewById(R.id.ivMin);
            plus    = itemView.findViewById(R.id.ivPlus);

            min.setOnClickListener(this);
            delete.setOnClickListener(this);
            plus.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivDel:
                    ((Activity)context).sendBroadcast(new Intent("removeProduk")
                            .putExtra("id", id));
                    break;
                case R.id.ivMin:
                    ((Activity)context).sendBroadcast(new Intent("minProduk")
                            .putExtra("id", id));
                    break;
                case R.id.ivPlus:
                    ((Activity)context).sendBroadcast(new Intent("plusProduk")
                            .putExtra("id", id));
                    break;
            }
        }
    }
}
