package adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ASUS on 09/11/2017.
 */

public class SpinnerData {

    ArrayList<Map<String, String>> spinnerListData;

    public void setData(ArrayList<Map<String, String>> listData) {
        spinnerListData = new ArrayList<Map<String, String>>();
        spinnerListData = listData;
    }

    public ArrayList<String> getListValue() {
        ArrayList<String> listValue = new ArrayList<String>();
        for (int i=0; i < spinnerListData.size(); i++) {
            Map<String, String> spinnerData = new HashMap<String, String>();
            spinnerData = spinnerListData.get(i);
            listValue.add(spinnerData.get("value"));
        }
        return listValue;
    }

    public ArrayList<String> getListCaption() {
        ArrayList<String> listCaption = new ArrayList<String>();
        for (int i=0; i < spinnerListData.size(); i++) {
            int a = i;
            Map<String, String> spinnerData = new HashMap<String, String>();
            spinnerData = spinnerListData.get(i);
            listCaption.add(spinnerData.get("caption"));
        }
        return listCaption;
    }

    public String getId(int index) {
        Map<String, String> spinnerData = new HashMap<String, String>();
        spinnerData = spinnerListData.get(index);
        return spinnerData.get("id");
    }

    public String getValue(int index) {
        Map<String, String> spinnerData = new HashMap<String, String>();
        spinnerData = spinnerListData.get(index);
        return spinnerData.get("value");
    }

    public String getCaption(int index) {
        Map<String, String> spinnerData = new HashMap<String, String>();
        spinnerData = spinnerListData.get(index);
        return spinnerData.get("caption");
    }

    public int getIndex(String value) {
        return getListValue().indexOf(value);
    }

    public ArrayList<Map<String, String>> getList() {
        return spinnerListData;
    }
}
